package com.ibarbu.books.data.pojo;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AuthorResponse {
    @SerializedName("page")
    public int page;
    @SerializedName("per_page")
    public int perPage;
    @SerializedName("total")
    public int total;
    @SerializedName("total_pages")
    public int totalPages;

    @SerializedName("data")
    public List<Author> authorList;

    @SuppressWarnings("InnerClassMayBeStatic")
    public class Author {
        @SerializedName("author")
        public String author;
        @SerializedName("title")
        public String title;
        @SerializedName("story_title")
        public String storyTitle;
    }
}



package com.ibarbu.books.data;

import com.ibarbu.books.data.pojo.AuthorResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface BooksApi {

    @GET("api/articles")
    Call<AuthorResponse> getAuthorsPage(@Query("page") int page);

    @GET("api/articles")
    Call<AuthorResponse> getBooksPage(@Query("page") int page, @Query("author") String author);
}

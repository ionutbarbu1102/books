package com.ibarbu.books.data;

import android.text.TextUtils;

import com.ibarbu.books.data.pojo.AuthorResponse;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Repository {

    private static Repository mInstance = null;

    private static final String BASE_URL = "https://jsonmock.hackerrank.com/";
    private final BooksApi booksApi;
    private final Set<String> mAuthorsSet = new LinkedHashSet<>();
    private final Map<String, Integer> mAuthorBooks = new HashMap<>();
    private final MutableLiveData<Map<String, Integer>> mAuthorsLiveData = new MutableLiveData<>();
    private final Object authorMutext = new Object();
    private final Object booksMutex = new Object();
    private final List<String> mBooksList = new LinkedList<>();
    private final MutableLiveData<List<String>> mBooksLiveData = new MutableLiveData<>();

    private Repository() {
        Retrofit retrofitClient = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        booksApi = retrofitClient.create(BooksApi.class);
    }

    public static Repository getInstance() {
        if (mInstance == null) {
            mInstance = new Repository();
        }
        return mInstance;
    }

    public LiveData<Map<String, Integer>> getAuthors() {
        new Thread(() -> {
            final AuthorResponse firstResponse = getAuthors(1);
            if (firstResponse != null) {
                synchronized (authorMutext) {
                    mAuthorsSet.clear();
                    final int pages = firstResponse.totalPages;
                    parseResponse(firstResponse);
                    int page = 2;
                    while (page <= pages) {
                        final AuthorResponse authorResponse = getAuthors(page);
                        if (authorResponse != null) {
                            parseResponse(authorResponse);
                        }
                        page++;
                    }
                }
                mAuthorsLiveData.postValue(mAuthorBooks);
            }
        }).start();
        return mAuthorsLiveData;
    }

    private void parseResponse(AuthorResponse authorResponse) {
        for (AuthorResponse.Author authorItem :
                authorResponse.authorList) {
            if (mAuthorsSet.add(authorItem.author)) {
                mAuthorBooks.put(authorItem.author, 1);
            } else {
                mAuthorBooks.put(authorItem.author, mAuthorBooks.get(authorItem.author) + 1);
            }
        }
    }

    @WorkerThread
    private AuthorResponse getAuthors(int page) {
        try {
            return booksApi.getAuthorsPage(page).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public LiveData<List<String>> getBooks(String author) {
        mBooksLiveData.postValue(Collections.emptyList());
        new Thread(() -> {
            final AuthorResponse firstResponse = getBooks(1, author);
            if (firstResponse != null) {
                synchronized (booksMutex) {
                    mBooksList.clear();
                    final int pages = firstResponse.totalPages;
                    parseResponseForBooks(firstResponse);
                    int page = 2;
                    while (page <= pages) {
                        final AuthorResponse authorResponse = getBooks(page, author);
                        if (authorResponse != null) {
                            parseResponseForBooks(authorResponse);
                        }
                        page++;
                    }
                }
                mBooksLiveData.postValue(mBooksList);
            }
        }).start();
        return mBooksLiveData;
    }

    @WorkerThread
    private AuthorResponse getBooks(int page, String author) {
        try {
            return booksApi.getBooksPage(page, author).execute().body();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void parseResponseForBooks(AuthorResponse authorResponse) {
        for (AuthorResponse.Author authorItem :
                authorResponse.authorList) {
            if (!TextUtils.isEmpty(authorItem.title)) {
                mBooksList.add(authorItem.title);
            } else if (!TextUtils.isEmpty(authorItem.storyTitle)) {
                mBooksList.add(authorItem.storyTitle);
            } else {
                mBooksList.add("N/A");
            }

        }
    }
}

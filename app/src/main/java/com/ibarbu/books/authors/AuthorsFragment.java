package com.ibarbu.books.authors;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibarbu.books.R;
import com.ibarbu.books.adapter.EntryAdapter;
import com.ibarbu.books.databinding.FragmentAuthorsBinding;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class AuthorsFragment extends Fragment {

    AuthorsViewModel authorsViewModel;

    public AuthorsFragment() {
        // Required empty public constructor
    }

    private final View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Bundle bundle = new Bundle();
            bundle.putString("author", (String)v.getTag());
            NavHostFragment.findNavController(AuthorsFragment.this).navigate(R.id.action_AuthorsFragment_to_BooksFragment, bundle);
        }
    };

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        authorsViewModel = new ViewModelProvider(this).get(AuthorsViewModel.class);
        FragmentAuthorsBinding binding = FragmentAuthorsBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        RecyclerView authorsRV = binding.getRoot().findViewById(R.id.authorsRV);

        authorsRV.setAdapter(new EntryAdapter(onClickListener));
        authorsRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        authorsViewModel.getAuthorsLiveData().observe(getViewLifecycleOwner(), new Observer<Map<String, Integer>>() {
            @Override
            public void onChanged(Map<String, Integer> authorMap) {
                ((EntryAdapter) Objects.requireNonNull(authorsRV.getAdapter())).updateItems(authorsViewModel.mapToList((HashMap<String, Integer>) authorMap));
            }
        });

        return binding.getRoot();
    }
}
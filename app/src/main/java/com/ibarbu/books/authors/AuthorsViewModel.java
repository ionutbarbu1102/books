package com.ibarbu.books.authors;

import com.ibarbu.books.data.Repository;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class AuthorsViewModel extends ViewModel {

    public LiveData<Map<String, Integer>> getAuthorsLiveData() {
        return Repository.getInstance().getAuthors();
    }

    public List<String> mapToList(HashMap<String, Integer> authorMap){
        LinkedList<String> resultList = new LinkedList();
        for (Map.Entry<String, Integer> mapItem :
                authorMap.entrySet()) {
            resultList.add(mapItem.getKey() + " : " + mapItem.getValue());
        }
        return resultList;
    }
}

package com.ibarbu.books.books;

import com.ibarbu.books.data.Repository;

import java.util.List;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;

public class BooksViewModel extends ViewModel {

    public LiveData<List<String>> getBooksLiveData(String author) {
        return Repository.getInstance().getBooks(author);
    }
}

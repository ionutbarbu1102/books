package com.ibarbu.books.books;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibarbu.books.R;
import com.ibarbu.books.adapter.EntryAdapter;
import com.ibarbu.books.databinding.FragmentBooksBinding;

import java.util.List;
import java.util.Objects;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class BooksFragment extends Fragment {

    private String mAuthor;
    private BooksViewModel booksViewModel;

    public BooksFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mAuthor = getArguments().getString("author");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        booksViewModel = new ViewModelProvider(this).get(BooksViewModel.class);
        FragmentBooksBinding binding = FragmentBooksBinding.inflate(inflater, container, false);
        binding.setLifecycleOwner(getViewLifecycleOwner());

        RecyclerView authorsRV = binding.getRoot().findViewById(R.id.booksRV);

        authorsRV.setAdapter(new EntryAdapter(null));
        authorsRV.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        booksViewModel.getBooksLiveData(mAuthor).observe(getViewLifecycleOwner(), (Observer<List<String>>) booksList ->
                ((EntryAdapter) Objects.requireNonNull(authorsRV.getAdapter())).updateItems(booksList));

        return binding.getRoot();
    }
}
package com.ibarbu.books.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibarbu.books.databinding.ItemAuthorBinding;

import java.util.LinkedList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class EntryAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private final List<String> mItems = new LinkedList<>();
    private final View.OnClickListener onClickListener;

    public EntryAdapter(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ItemAuthorBinding binding = ItemAuthorBinding.inflate(layoutInflater, parent, false);

        if (onClickListener != null) {
            binding.itemContainer.setOnClickListener(onClickListener);
        }
        return new ItemViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        holder.getBinding().setAuthorName(mItems.get(position));
        if (onClickListener != null) {
            String authorName = mItems.get(position).substring(0, mItems.get(position).indexOf(":")).trim();
            holder.getBinding().itemContainer.setTag(authorName);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    public void updateItems(List<String> items) {
        mItems.clear();
        mItems.addAll(items);
        notifyDataSetChanged();
    }

}

class ItemViewHolder extends RecyclerView.ViewHolder {


    private ItemAuthorBinding mBinding;

    public ItemViewHolder(@NonNull ItemAuthorBinding binding) {
        super(binding.getRoot());
        mBinding = binding;
    }

    public ItemAuthorBinding getBinding() {
        return mBinding;
    }

    public void setBinding(ItemAuthorBinding mBinding) {
        this.mBinding = mBinding;
    }
}

package com.ibarbu.books;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.ibarbu.books.data.Repository;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
}